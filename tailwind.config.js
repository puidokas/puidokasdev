/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'plus': "url('/icons/plus.svg')",
        'diagonal': "url('/icons/diagonal.svg')"
      },
      animation: {
        "text-gradient": "text-gradient 3s linear infinite"
      },
      keyframes: {
        "text-gradient": {
          "to": {
            "backgroundPosition": "200% center"
          }
        }
      },
    },
  },
  plugins: [],
}

