'use client';

import Link from "next/link";
import { useRef } from "react";
import ExternalLink from "../shared/externalLink";
import HeaderItem from "./headerItem";

export default function Header() {
    function EducationSvg() {
        return (<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6 mx-auto">
        <path d="M11.7 2.805a.75.75 0 01.6 0A60.65 60.65 0 0122.83 8.72a.75.75 0 01-.231 1.337 49.949 49.949 0 00-9.902 3.912l-.003.002-.34.18a.75.75 0 01-.707 0A50.009 50.009 0 007.5 12.174v-.224c0-.131.067-.248.172-.311a54.614 54.614 0 014.653-2.52.75.75 0 00-.65-1.352 56.129 56.129 0 00-4.78 2.589 1.858 1.858 0 00-.859 1.228 49.803 49.803 0 00-4.634-1.527.75.75 0 01-.231-1.337A60.653 60.653 0 0111.7 2.805z" />
        <path d="M13.06 15.473a48.45 48.45 0 017.666-3.282c.134 1.414.22 2.843.255 4.285a.75.75 0 01-.46.71 47.878 47.878 0 00-8.105 4.342.75.75 0 01-.832 0 47.877 47.877 0 00-8.104-4.342.75.75 0 01-.461-.71c.035-1.442.121-2.87.255-4.286A48.4 48.4 0 016 13.18v1.27a1.5 1.5 0 00-.14 2.508c-.09.38-.222.753-.397 1.11.452.213.901.434 1.346.661a6.729 6.729 0 00.551-1.608 1.5 1.5 0 00.14-2.67v-.645a48.549 48.549 0 013.44 1.668 2.25 2.25 0 002.12 0z" />
        <path d="M4.462 19.462c.42-.419.753-.89 1-1.394.453.213.902.434 1.347.661a6.743 6.743 0 01-1.286 1.794.75.75 0 11-1.06-1.06z" />
      </svg>);}

    function EmploymentSvg() {
        return (<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6 mx-auto">
        <path fillRule="evenodd" d="M7.5 5.25a3 3 0 013-3h3a3 3 0 013 3v.205c.933.085 1.857.197 2.774.334 1.454.218 2.476 1.483 2.476 2.917v3.033c0 1.211-.734 2.352-1.936 2.752A24.726 24.726 0 0112 15.75c-2.73 0-5.357-.442-7.814-1.259-1.202-.4-1.936-1.541-1.936-2.752V8.706c0-1.434 1.022-2.7 2.476-2.917A48.814 48.814 0 017.5 5.455V5.25zm7.5 0v.09a49.488 49.488 0 00-6 0v-.09a1.5 1.5 0 011.5-1.5h3a1.5 1.5 0 011.5 1.5zm-3 8.25a.75.75 0 100-1.5.75.75 0 000 1.5z" clipRule="evenodd" />
        <path d="M3 18.4v-2.796a4.3 4.3 0 00.713.31A26.226 26.226 0 0012 17.25c2.892 0 5.68-.468 8.287-1.335.252-.084.49-.189.713-.311V18.4c0 1.452-1.047 2.728-2.523 2.923-2.12.282-4.282.427-6.477.427a49.19 49.19 0 01-6.477-.427C4.047 21.128 3 19.852 3 18.4z" />
        </svg>
    );}
    
    function LanguageSvg() {
        return (<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6 mx-auto">
        <path fillRule="evenodd" d="M9 2.25a.75.75 0 01.75.75v1.506a49.38 49.38 0 015.343.371.75.75 0 11-.186 1.489c-.66-.083-1.323-.151-1.99-.206a18.67 18.67 0 01-2.969 6.323c.317.384.65.753.998 1.107a.75.75 0 11-1.07 1.052A18.902 18.902 0 019 13.687a18.823 18.823 0 01-5.656 4.482.75.75 0 11-.688-1.333 17.323 17.323 0 005.396-4.353A18.72 18.72 0 015.89 8.598a.75.75 0 011.388-.568A17.21 17.21 0 009 11.224a17.17 17.17 0 002.391-5.165 48.038 48.038 0 00-8.298.307.75.75 0 01-.186-1.489 49.159 49.159 0 015.343-.371V3A.75.75 0 019 2.25zM15.75 9a.75.75 0 01.68.433l5.25 11.25a.75.75 0 01-1.36.634l-1.198-2.567h-6.744l-1.198 2.567a.75.75 0 01-1.36-.634l5.25-11.25A.75.75 0 0115.75 9zm-2.672 8.25h5.344l-2.672-5.726-2.672 5.726z" clipRule="evenodd" />
      </svg>      
    );}

    const HeaderItems = [
        {
            Icon: LanguageSvg,
            headline: "Languages",
            content: `Fluent: English, Danish, Lithuanian
            Limited: German, Norwegian, Swedish, Russian`,
        },
        {
            Icon: EducationSvg,
            headline: "Education",
            content: `Computer Science/Data Science`,
        },
        {
            Icon: EmploymentSvg,
            headline: "Experience",
            content: `10+ years within IT`,
        },
    ];

    const dropdownRef = useRef(null);
    const handleCvClick = (e) => {
        e?.preventDefault();

        if (dropdownRef.current.classList.contains("hidden")) {
            dropdownRef.current.classList.remove("hidden");
        } else {
            dropdownRef.current.classList.add("hidden");
        }
      };

    return (
        <header className="text-slate-200 flex flex-col items-center p-5 pt-12 pb-12 bg-[radial-gradient(ellipse_at_center,_var(--tw-gradient-stops))] from-purple-950/50 to-black">
            <div className="flex flex-row">
                <div className="rounded-full overflow-hidden">
                    <img className="h-40 w-40 object-cover" src="/images/jonas.jpg"/>
                </div>
                <div className="mx-5 my-auto">      
                    <h1 className="font-bold text-2xl lg:text-4xl mb-3">Jonas Puidokas</h1>
                    <h2 className="font-semibold mb-3">Software Developer</h2>
                    <div className="flex flex-row">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6 mr-1">
                        <path fillRule="evenodd" d="M11.54 22.351l.07.04.028.016a.76.76 0 00.723 0l.028-.015.071-.041a16.975 16.975 0 001.144-.742 19.58 19.58 0 002.683-2.282c1.944-1.99 3.963-4.98 3.963-8.827a8.25 8.25 0 00-16.5 0c0 3.846 2.02 6.837 3.963 8.827a19.58 19.58 0 002.682 2.282 16.975 16.975 0 001.145.742zM12 13.5a3 3 0 100-6 3 3 0 000 6z" clipRule="evenodd" />
                        </svg>
                        <h3>Copenhagen, Denmark</h3>
                    </div>
                </div>
            </div>
        <div className="my-8 lg:my-16 flex flex-col md:flex-row w-full lg:w-3/4 md:gap-3">
        {HeaderItems.map((project, i) => {     
                    return (
                        <HeaderItem key={i}
                            {...project}
                        />
                    )})}
        </div>
        <div className="flex flex-col md:flex-row gap-5">
            <div>
                <ExternalLink label={"View LinkedIn"} link={process.env.linkedin} large={true}></ExternalLink>
            </div>
            <div>
                <Link href="" className="inline-flex hover:no-underline group" onClick={(e) => handleCvClick(e)}>
                    <span className={'text-lg border-2 px-4 py-2 inline-flex cursor-pointer items-center justify-center rounded-full border-slate-800 hover:border-slate-300 group bg-black font-medium text-slate-300 backdrop-blur-3xl'}>
                        <span className='bg-gradient-to-t from-blue-300 to-purple-500 bg-clip-text text-transparent group-hover:text-slate-300'>
                            Download CV
                        </span>
                        <svg className="fill-current text-gray-200" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                            <path d="M7 10l5 5 5-5z" />
                            <path d="M0 0h24v24H0z" fill="none" />
                        </svg>
                    </span>
                </Link>
                <div ref={dropdownRef} className={"py-2 rounded-lg shadow-xl bg-black hidden"}> 
                    <Link href="/pdf/CV-Jonas_Puidokas_EN.pdf" target="_blank" className="block px-4 py-2 text-slate-300 hover:bg-indigo-500 hover:text-white">English</Link>
                    <Link href="/pdf/CV-Jonas_Puidokas_DA.pdf" target="_blank" className="block px-4 py-2 text-slate-300 hover:bg-indigo-500 hover:text-white">Danish</Link>
                </div>
            </div>            
        </div>
        </header>
    );
}