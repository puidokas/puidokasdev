import ExternalLink from "../shared/externalLink";

export default function Footer() {
    return (
        <footer className="mt-14 lg:mt-28">
        <div className="container mx-auto border-t border-slate-200 p-8 text-center border-slate-900 md:flex-row md:px-12">
          &copy; {new Date().getFullYear()} Jonas Puidokas. The website is powered by <ExternalLink label={"NextJS"} link={"https://nextjs.org/"} inline={true}/>, <ExternalLink label={"ReactJS"} link={"https://react.dev/"} inline={true}/> and <ExternalLink label={"Tailwind CSS"} link={"https://tailwindcss.com/"} inline={true}/>. The code is freely available on <ExternalLink label={"GitLab"} link={"https://gitlab.com/puidokas/puidokasdev"} inline={true}/>.
        </div>
      </footer>
    );
}