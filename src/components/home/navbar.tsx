import Link from "next/link";

export default function NavBar() {
  const links = [
    {
      label: "Home",
      href: "#"
    },
    {
      label: "About",
      href: "#about"
    },
    {
      label: "Code",
      href: "#code"
    },
    {
      label: "Contact",
      href: "#contact"
    },
  ];

    return (
        <nav className="bg-diagonal flex flex-col md:flex-row md:justify-between p-3">
          <span className="mx-auto md:mx-0 uppercase font-black inline-flex animate-text-gradient bg-gradient-to-r from-purple-700 via-blue-600 to-purple-700 bg-[200%_auto] bg-clip-text text-xl text-transparent">Puidokas.dev</span>
          <ul className="flex flex-row font-medium mx-auto md:mx-0">
          {links.map((link, i) => {     
                    return (
                      <li key={i} className="rounded-lg px-3 py-1 hover:bg-blue-800">
                        <Link href={link.href}>{link.label}</Link>
                      </li>
                    )})}
          </ul>
      </nav>
    );
}