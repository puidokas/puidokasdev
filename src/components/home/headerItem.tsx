export default function HeaderItem({Icon, headline, content}) {
    return (
        <div className="flex flex-col w-full">
            <Icon/>
            <p className="text-center mt-3 mb-2 text-xl font-bold leading-tight text-slate-50 md:text-xl">{headline}</p>
            <p className="text-center" style={{ whiteSpace: "pre-line" }}>{content}</p>
        </div>
    );
}