import ExternalLink from "../shared/externalLink";
import Section from "../shared/section";
import Project from "./project";

export default function Code() {
    const projects = [
        {
            title: "Personal website",
            description: "This website is freely accessible on GitLab. It was created with the dual purpose of establishing an online presence for self-presentation and showcasing my frontend skills.",
            langs: "NextJS, ReactJS, TailwindCSS, Typescript",
            link: "https://gitlab.com/puidokas/puidokasdev"
        },
        {
            title: "Scraper script",
            description: "This script logs into an ASP.NET website, extracts data from multiple web pages, invokes a backend API endpoint for each page, and generates an HTML table featuring images, descriptions, and links. The scraping process occurs pre-Javascript loading. Please note that this script is intended solely for educational purposes.",
            langs: "Python, HTML",
            link: "https://gitlab.com/puidokas/scraper_pre-js"
        },
        {
            title: "Minimalistic website",
            description: "A simple personal website developed in ReactJS, and it is publicly available on GitLab. This website incorporates features like instant language change and dynamic content loading. The web design adheres to a minimalist approach.",
            langs: "ReactJS, HTML, JS, CSS",
            link: "https://gitlab.com/puidokas/jnsh.gitlab.io"
        },
        {
            title: "Wish list app",
            description: "A simple wish list app that was developed to learn Angular.",
            langs: "Angular, HTML, JS, CSS",
            link: "https://gitlab.com/puidokas/sample-angular"
        },
        {
            title: "Advent of Code",
            description: "Advent of Code is an annual coding challenge where developers from around the world engage in logic challenges shortly before Christmas. Participants attempt to solve these challenges by crafting algorithms in code.",
            langs: "Python",
            link: "https://gitlab.com/puidokas/advent_of_code-2018"
        },
        {
            title: "SOLID Principles",
            description: "This project serves as a demonstration of various SOLID principles in software development, featuring practical examples to illustrate their application.",
            langs: "C#",
            link: "https://gitlab.com/puidokas/solid_principles"
        },
    ];

    const technologies = [
        {
            title: "Languages",
            tech: "Java, C#, Python, HTML, XML, CSS, JS, Apex, Gosu, Groovy"
        },
        {
            title: "Database",
            tech: "Microsoft SQL Server (MSSQL), MySQL, MongoDB, Entity Framework"
        },
        {
            title: "Framework",
            tech: "ReactJS, NextJS, Angular, ASP.NET, Grails, Jakarta Enterprise Beans (JEB/EJB)"
        },       
        {
            title: "DevOps",
            tech: "Jenkins, Docker, Kubernetes, Kibana, Elasticsearch, Tomcat, Maven, Gradle"
        },
        {
            title: "Code quality",
            tech: "SonarQube"
        }, 
        {
            title: "Platform",
            tech: "Salesforce, Guidewire"
        },        
        {
            title: "Big Data management",
            tech: "PowerBI, Alteryx, Tableau, R Studio"
        },            
        {
            title: "Event architecture",
            tech: "Apache ActiveMQ (AMQ), Apache Camel, Kafka, Java Message Service (JMS)"
        },        
        {
            title: "Source code management",
            tech: "Git, Bitbucket"
        },        
        {
            title: "Work item management",
            tech: "Jira, Azure DevOps, Confluence"
        },
        {
            title: "Security",
            tech: "Keycloak, Passwordstate"
        },
        {
            title: "Test framework",
            tech: "Cucumber, JUnit, Spock, Mockito, Mockery"
        },
        {
            title: "Test",
            tech: "Integration testing, unit testing, smoke testing, data-driven testing, behavior-driven testing, behavior-driven development, acceptance testing, keyword-driven testing, regression testing, mocks"
        },
        {
            title: "Project/Team management",
            tech: "Agile, SCRUM, Waterfall, SAFe"
        },
        {
            title: "Operating systems",
            tech: "Linux, Windows, LDAP, Active Directory, VMWare"
        },
    ];

    return (
        <Section id="code" title="Code">
            <div className="flex flex-col lg:flex-row">
                <div className="basis-1/2 lg:mr-10 mb-10 lg:mb-0">
                    <p>Most of the code I have written was during employment, unfortunately, it is confidential and not publicly available. However, you can explore some of my hobby projects on <ExternalLink label={"GitLab"} link={"https://gitlab.com/puidokas/puidokasdev"} inline={true}/>. For instance, this website is constructed using NextJS/ReactJS and Tailwind CSS, with the code being publicly accessible.</p>
                    <p>The following is a non-exhaustive list of some technologies I have worked with. While I have experience with some technologies for an extended period, it may have been a long time ago, and with others, it may have been more recently, so proficiency levels can vary. Nevertheless, I can quickly reacquaint myself with any technology I have worked with in the past.</p>
                    {technologies.map((tech, i) => {     
                    return (
                        <div className="grid grid-cols-2" key={i}>
                            <h3 className="font-bold">{tech.title}</h3>
                            <p>{tech.tech}</p>
                        </div>
                    )})}
                </div>
                <div className="grid md:grid-cols-2 basis-1/2 gap-2 lg:gap-8">
                {projects.map((project, i) => {     
                    return (
                        <Project key={i}
                            {...project}
                        />
                    )})}
                </div>
            </div>
        </Section>
    );
}