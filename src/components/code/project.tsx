import ExternalLink from "../shared/externalLink";

export default function Project({title, description, langs, link}) {
    return (
        <div className="rounded-xl border border-slate-800 bg-gradient-to-r from-black to-slate-950 p-5">
            <h1 className="text-xl font-semibold mb-4">{title}</h1>
            <p className="mb-4">{description}</p>
            <p className="mb-4">Coded in: {langs}</p>
            <ExternalLink label={"Repository"} link={link}></ExternalLink>
        </div>
    );
}