import Link from "next/link";

export default function ExternalLink({label, link, external = true, large = false, inline = false}) {
    let externalIcon = 
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6 ml-1">
        <path fillRule="evenodd" d="M15.75 2.25H21a.75.75 0 01.75.75v5.25a.75.75 0 01-1.5 0V4.81L8.03 17.03a.75.75 0 01-1.06-1.06L19.19 3.75h-3.44a.75.75 0 010-1.5zm-10.5 4.5a1.5 1.5 0 00-1.5 1.5v10.5a1.5 1.5 0 001.5 1.5h10.5a1.5 1.5 0 001.5-1.5V10.5a.75.75 0 011.5 0v8.25a3 3 0 01-3 3H5.25a3 3 0 01-3-3V8.25a3 3 0 013-3h8.25a.75.75 0 010 1.5H5.25z" clipRule="evenodd" />
    </svg>;

    if (!external) {
        externalIcon = null;
    } 

    let button : string;
    if (large) {
        button = 'text-lg border-2 px-4 py-2'
    } else {
        if (inline) {
            button = ''
            externalIcon = null;
        } else {
            button = 'text-sm border px-4 py-2'
        }
    }

    return (
        <Link href={link} rel="noopener noreferrer" target={"_blank"} className="inline-flex hover:no-underline group">
            <span className={button + ' inline-flex cursor-pointer items-center justify-center rounded-full border-slate-800 hover:border-slate-300 group bg-black font-medium text-slate-300 backdrop-blur-3xl'}>
                <span className='bg-gradient-to-t from-blue-300 to-purple-500 bg-clip-text text-transparent group-hover:text-slate-300'>
                    {label}
                </span>
                {externalIcon}
            </span>
        </Link>

    );
}