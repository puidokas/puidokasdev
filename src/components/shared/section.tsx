import Link from "next/link";

export default function Section(props) {
    return (
        <section id={props.id} className="flex flex-col items-center px-4 lg:px-10">
            <div className="flex w-full py-5 lg:py-10">
                    <h1 className="text-4xl font-bold mx-auto">{props.title}</h1>
                    <div className="relative">
                        <div className="absolute right-0">
                            <Link href="#top">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6 hover:fill-blue-500">
                                    <path fillRule="evenodd" d="M11.47 4.72a.75.75 0 011.06 0l7.5 7.5a.75.75 0 11-1.06 1.06L12 6.31l-6.97 6.97a.75.75 0 01-1.06-1.06l7.5-7.5zm.53 7.59l-6.97 6.97a.75.75 0 01-1.06-1.06l7.5-7.5a.75.75 0 011.06 0l7.5 7.5a.75.75 0 11-1.06 1.06L12 12.31z" clipRule="evenodd" />
                                </svg>
                            </Link>
                        </div>
                    </div>

            </div>
            {props.children}
        </section>
    );
}