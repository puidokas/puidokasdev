import TimelinePoint from "./timelinePoint";
import Section from "../shared/section";
import ExternalLink from "../shared/externalLink";

export default function About() {
    const timelinePoints = [
        {
            title: "Software Development Consultant at Netcompany",
            description: "Worked at one of the leading IT consultancy companies and had the pleasure of engaging with various projects and customers. This experience exposed me to different tech stacks and work methodologies.",
            years: "2018-2024",
            label: "Link to Netcompany",
            link: "https://www.netcompany.com/"
        },
        {
            title: "Master's in Data Science",
            description: "Started a Master's in Computer Science but found it to be repetitive of my existing knowledge. Consequently, I switched to Data Science at another leading university in Denmark because Data Science/Machine Learning was gaining popularity and was relevant for my future. I completed courses in data science, machine learning, and e-business. Additionally, I collaborated with one of the leading telecommunication companies to train an AI model for automatically categorizing incoming emails.",
            years: "2016-2018",
            label: "Link to University",
            link: "https://studieordninger.cbs.dk/2017/kan/291"
        },
        {
            title: "Software Developer at Siemens",
            description: "Worked full-time for one year and part-time for two years as a software developer in an Agile team. We developed software for modern traffic management systems, including the control of train traffic.",
            years: "2015-2018",
            label: "Link to Siemens",
            link: "https://www.mobility.siemens.com/dk/da.html"
        },
        {
            title: "Bachelor's in Computer Science",
            description: "Studied computer algorithms, cybersecurity, and software development at the leading technical university in Denmark.",
            years: "2012-2015",
            label: "Link to University",
            link: "https://www.compute.dtu.dk/om-os/bibliotek/diplom-it"
        },
        {
            title: "Freelance Software Development",
            description: "Developed several websites coded in HTML, JS, CSS, and PHP and set up various content management systems, including WordPress, Drupal, and Joomla. While most projects were pursued for enjoyment, I often found myself growing bored with the maintenance, leading to either abandonment or sale. For instance, I sold my previous website, Minecraft.lt, which was built on WordPress.",
            years: "2000-2011",
            label: "Link to archived Minecraft.lt",
            link: "http://web.archive.org/web/20111003154752/http://www.minecraft.lt/"
        },
    ];
    return (
        <Section id="about" title="About">
            <div className="flex flex-col lg:flex-row">
                 <div className="flex justify-center lg:mr-10 mb-10 lg:mb-0">
                     <div className="space-y-6 border-l-2 border-dashed">
                         {timelinePoints.map((point, i) => {     
                        return (
                            <TimelinePoint key={i}
                                title={point.title}
                                description={point.description}
                                years={point.years}
                                label={point.label}
                                link={point.link}
                            />
                        ) 
                        })}
                    </div>
                </div>
                <div className="basis-1/2">
                    <p>An ambitious and committed software engineer, who brings a wealth of experience from diverse professional settings, including freelancing, startups, large corporations, and IT consultancy. Holding top grades in both Computer Science and E-Business/Data Science degrees, I have demonstrated my dedication to academic excellence.</p>
                    <p>Originally from Lithuania, I made a bold move to Denmark at the age of 20, showcasing my adaptability and linguistic prowess as I am fluent in English, Danish, and Lithuanian. With additional knowledge in German, Norwegian, Swedish and Russian, I am eager to further develop my language skills.</p>
                    <p>My expertise lies in computer systems and software design, honed through active involvement in the development of IT systems requiring proficiency in software development, machine learning, and computer networking. My passion lies in constructing scalable and efficient software solutions that address specific challenges, achieved through an agile approach and close collaboration with customers.</p>
                    <p>A proactive individual unafraid of responsibility, I consistently deliver results on time, ensuring agreed-upon quality and pricing. I am now seeking opportunities in backend software development, machine learning, and data analytics. I am particularly interested in collaborating with ambitious, talented individuals in companies that prioritize employee value and aim to make a positive impact.</p>
                    <p>If you are looking for a dedicated software engineer with a strong academic background, a proven track record in diverse professional environments, and a passion for innovation, I am excited about the prospect of contributing to and thriving within your dynamic team. Let's connect and explore how my skills can complement your organization's goals.</p>
                    <p>Please visit my <ExternalLink label={"LinkedIn"} link={process.env.linkedin} inline={true}/> for more information about my employment experience and professional skills.</p>
                </div>
            </div>
        </Section>
    );
}