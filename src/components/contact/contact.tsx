import Section from "../shared/section";
import ExternalLink from "../shared/externalLink";

export default function Contact() {
    return (
        <Section id="contact" title="Contact">
            <div>
                <p>You can contact me on <ExternalLink label={"LinkedIn"} link={process.env.linkedin} inline={true}></ExternalLink>. Please state that you visited this website.</p>
            </div>
        </Section>
    );
}