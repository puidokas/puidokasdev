import type { Metadata } from 'next'
import { Inter } from 'next/font/google'

import './tw_input.css'
import './tw_output.css'

export const metadata: Metadata = {
  title: 'Jonas Puidokas',
  description: 'Personal website by Jonas Puidokas',
}

const inter = Inter({
  subsets: ['latin'],
  display: 'swap',
})

export default function RootLayout({
    children,
  }: {
    children: React.ReactNode
  }) {
    return (
      <html lang="en" className={inter.className + " bg-slate-950 bg-plus"}>
        <head>
          <script defer src='https://static.cloudflareinsights.com/beacon.min.js' data-cf-beacon='{"token": "9cea31b95ed443509e64e4decb55bc4f"}'></script>
          <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png"/>
          <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png"/>
          <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png"/>
          <link rel="manifest" href="/favicon/site.webmanifest"/>
          <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5"/>
          <meta name="msapplication-TileColor" content="#da532c"/>
          <meta name="theme-color" content="#ffffff"/>
        </head>
        <body className="lg:container lg:mx-auto bg-black text-slate-200">{children}</body>
      </html>
    )
  }