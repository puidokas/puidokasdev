import About from "../components/about/about";
import Contact from "../components/contact/contact";
import Footer from "../components/home/footer";
import Header from "../components/home/header";
import NavBar from "../components/home/navbar";
import Code from "../components/code/code";

export default function Page() {
  return [
    <NavBar></NavBar>,
    <Header></Header>,
    <About></About>,
    <Code></Code>,
    <Contact></Contact>,
    <Footer></Footer>
  ];
}